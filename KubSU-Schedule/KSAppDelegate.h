//
//  KSAppDelegate.h
//  KubSU-Schedule
//
//  Created by Eugene Butusov on 19.09.14.
//  Copyright (c) 2014 KubSU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
