//
// Created by Eugene Butusov on 19.09.14.
// Copyright (c) 2014 KubSU. All rights reserved.
//

#import "KSViewController.h"
#import "KSCommandsPool.h"


@interface KSViewController () <UIGestureRecognizerDelegate>

@end


@implementation KSViewController {

}

-(void)dismissKeyboard:(id)sender {
    [self.view endEditing:YES];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return self.keyboardAvoiding;
}

-(void)viewDidLoad {
    [super viewDidLoad];

    self.keyboardAvoiding = YES;
    UITapGestureRecognizer *keyboardAvoider = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                      action:@selector(dismissKeyboard:)];
    keyboardAvoider.delegate = self;
    [self.view addGestureRecognizer:keyboardAvoider];

    self.commandsPool = [KSCommandsPool pool];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)showAlertWithError:(NSError *)error
{
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Ошибка", nil)
                                message:error.localizedDescription delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [self.commandsPool revokeAllCommands];
    [super viewDidDisappear:animated];
}

@end