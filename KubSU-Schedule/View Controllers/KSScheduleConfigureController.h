//
// Created by Eugene Butusov on 08.05.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KSViewController.h"

@class KSUnion;


@interface KSScheduleConfigureController : KSViewController

@property (nonatomic, strong) NSDate *from;
@property (nonatomic, strong) NSDate *to;
@property (nonatomic, strong) KSUnion *anUnion;

@property (nonatomic, strong) void(^completionHandler)();

@end