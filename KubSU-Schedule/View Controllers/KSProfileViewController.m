//
// Created by Eugene Butusov on 05.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSProfileViewController.h"
#import "KSApplicationManager.h"
#import "KSProfileHeaderView.h"
#import "KSKeyValueCell.h"
#import "KSUser.h"


@implementation KSProfileViewController {

}

-(void)viewDidLoad {
    [super viewDidLoad];

    self.title = NSLocalizedString(@"Профиль", nil);
    __block void(^profileUpdateBlock)() = ^{
        self.tableView.tableHeaderView = [[KSProfileHeaderView alloc] initWithTableView:self.tableView
                                                                             andProfile:[KSApplicationManager manager].me];
    };
    if (![KSApplicationManager manager].me) {

    } else {
        profileUpdateBlock();
    }
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
            initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                 target:self action:@selector(openActions:)];
}

-(void)openActions:(id)sender {
    [[[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Выберите действие", nil) delegate:nil
                        cancelButtonTitle:NSLocalizedString(@"Отмена", nil)
                   destructiveButtonTitle:NSLocalizedString(@"Выйти", nil)
                        otherButtonTitles:nil] showInView:self.view];
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *keyValueIdentifier = @"keyValueIdentifier";
    KSKeyValueCell *cell = [tableView dequeueReusableCellWithIdentifier:keyValueIdentifier];
    if (!cell) {
        cell = [[KSKeyValueCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:keyValueIdentifier];
    }
    switch (indexPath.row) {
        case 0:
            cell.key = NSLocalizedString(@"Логин", nil);
            cell.value = [KSApplicationManager manager].me.username;
            break;
        case 1:
            cell.key = NSLocalizedString(@"E-Mail", nil);
            cell.value = [KSApplicationManager manager].me.email;
            break;
        default:
            break;
    }
    return cell;
}

@end