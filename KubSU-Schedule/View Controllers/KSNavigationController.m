//
// Created by Eugene Butusov on 05.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSNavigationController.h"


@implementation KSNavigationController {

}

-(void)viewDidLoad {
    [super viewDidLoad];

    [self setNeedsStatusBarAppearanceUpdate];
    self.navigationBar.barTintColor = [UIColor colorWithRed:45.0f / 255.0f green:72.0f / 255.0f blue:147.0f / 255.0f
                                                      alpha:0.7f];
    self.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationBar setTitleTextAttributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor],
            NSFontAttributeName : [UIFont systemFontOfSize:22.0f] }];
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end