//
// Created by Eugene Butusov on 06.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSUnionSelectorController.h"
#import "KSUnion.h"


@implementation KSUnionSelectorController {
    NSArray *_dataSource;
}

-(void)setCurrent:(KSUnion *)current {
    if (_current != current) {
        _current = current;
    }

    [self buildDataSource];
}

-(void)setAvailableUnions:(NSArray *)availableUnions {
    if (_availableUnions != availableUnions) {
        _availableUnions = availableUnions;
    }

    [self buildDataSource];
}

-(BOOL)hasChildren:(KSUnion *)anUnion {
    for (KSUnion *item in _availableUnions) {
        if (item.parent == anUnion)
            return YES;
    }
    return NO;
}

-(void)buildDataSource {
    if (_availableUnions) {
        _dataSource = [_availableUnions objectsAtIndexes:[_availableUnions indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            return ((KSUnion *)obj).parent == self.current;
        }]];
        [self.tableView reloadData];
    }
}

-(void)viewDidLoad {
    [super viewDidLoad];

    self.keyboardAvoiding = NO;
    self.title = self.current ? self.current.name : NSLocalizedString(@"Объединения", nil);
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                          target:self action:@selector(doCancel:)];
}

-(void)doCancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSource.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.textLabel.font = [UIFont systemFontOfSize:14.0f];
    cell.textLabel.text = ((KSUnion *)_dataSource[indexPath.row]).name;
    cell.detailTextLabel.text = ((KSUnion *)_dataSource[indexPath.row]).description;
    cell.accessoryType = [self hasChildren:_dataSource[indexPath.row]] ?
            UITableViewCellAccessoryDetailDisclosureButton : UITableViewCellAccessoryNone;
    return cell;
}

-(void)tableView :(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self dismissViewControllerAnimated:YES completion:^{
        if (self.completionBlock) {
            self.completionBlock(_dataSource[indexPath.row]);
        }
    }];
}

-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    KSUnionSelectorController *selectorController = [[KSUnionSelectorController alloc] init];
    selectorController.current = _dataSource[indexPath.row];
    selectorController.availableUnions = _availableUnions;
    selectorController.completionBlock = self.completionBlock;
    [self.navigationController pushViewController:selectorController animated:YES];
}

@end