//
// Created by Eugene Butusov on 24.05.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSNewsDetailsViewController.h"
#import "KSNotification.h"
#import "KSUser.h"


@implementation KSNewsDetailsViewController {
    IBOutlet UILabel *_titleLabel;
    IBOutlet UILabel *_subtitleLabel;

    IBOutlet UITextView *_contentView;

    NSDateFormatter *_dateFormatter;
}

-(void)viewDidLoad {
    [super viewDidLoad];

    self.title = NSLocalizedString(@"Уведомление", nil);
    _dateFormatter = [NSDateFormatter new];
    _dateFormatter.dateFormat = @"dd.MM.yyyy HH:mm";
    self.news = _news;
}

-(void)setNews:(KSNotification *)news {
    if (_news != news) {
        _news = news;
    }

    _titleLabel.text = _news.title;
    _subtitleLabel.text = [NSString stringWithFormat:@"%@ %@.%@., %@", _news.author.lastName,
                    [_news.author.firstName substringToIndex:1], [_news.author.givenName substringToIndex:1],
                    [_dateFormatter stringFromDate:_news.at]];
    _contentView.text = _news.content;
    [_contentView scrollRangeToVisible:NSMakeRange(0, 0)];
}

@end