//
// Created by Eugene Butusov on 24.05.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KSViewController.h"

@class KSNotification;


@interface KSNewsDetailsViewController : KSViewController

@property (nonatomic, strong) KSNotification *news;

@end