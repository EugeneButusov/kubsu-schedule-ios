//
// Created by Eugene Butusov on 05.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSSignupViewController.h"
#import "KSStringInputCell.h"
#import "KSUser.h"
#import "KSMe.h"
#import "KSAddUnionCell.h"
#import "KSUnionCell.h"
#import "KSMetadataCommand.h"
#import "KSCommandsPool.h"
#import "MBProgressHUD.h"
#import "KSUnionSelectorController.h"
#import "KSNavigationController.h"
#import "MTVariantsKeyboard.h"
#import "KSProfileCommand.h"

@interface KSSignupViewController () <VariantsKeyboardDelegate>

@end


@implementation KSSignupViewController {
    KSMe *_me;
    NSArray *_availableUnions;

    MTVariantsKeyboard *_genderKeyboard;
}

-(void)viewDidLoad {
    [super viewDidLoad];

    self.title = NSLocalizedString(@"Заявка на доступ", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Готово", nil)
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self action:@selector(doSignup:)];
    _me = [KSMe new];
    _genderKeyboard = [[MTVariantsKeyboard alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 0)];
    _genderKeyboard.delegate = self;
    _genderKeyboard.datasource = @[@"Студент", @"Преподаватель"];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    if (!_availableUnions) {
        __block MBProgressHUD *alert = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        alert.dimBackground = YES;
        alert.labelText = NSLocalizedString(@"Загрузка...", nil);
        KSMetadataCommand *command = [KSMetadataCommand getAllUnionsWithSuccessBlock:^(KSCommonCommand *command) {
            [alert hide:YES];
            _availableUnions = (NSArray *)command.result;
        } errorBlock:^(NSError *error) {
            [alert hide:YES];
            [self showAlertWithError:error];
        }];
        [self.commandsPool setAndExecuteCommand:command];
    }
}

-(void)startApplication:(id)sender
{
    UIWindow *currentWindow = (UIWindow *)[UIApplication sharedApplication].windows[0];
    CATransition *animation = [CATransition animation];
    animation.type = kCATransitionReveal;
    animation.subtype = kCATransitionFromTop;

    [currentWindow.layer removeAllAnimations];
    [currentWindow.layer addAnimation:animation forKey:kCATransition];

    currentWindow.rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"rootView"];
}

-(void)doSignup:(id)sender {
    [self.view endEditing:YES];
    __block MBProgressHUD *alert = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    alert.dimBackground = YES;
    alert.labelText = NSLocalizedString(@"Регистрация...", nil);
    KSProfileCommand *command = [KSProfileCommand updateProfileForMe:_me withSuccessBlock:^(KSCommonCommand *command) {
        [alert hide:YES];
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Ошибка", nil)
                                    message:NSLocalizedString(@"Заявка на создание была отправлена. "
                                            "Как только аккаунт будет активирован администратором, Вы получите уведомление.", nil)
                                   delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
    } errorBlock:^(NSError *error) {
        [alert hide:YES];
        [self showAlertWithError:error];
    }];
    [self.commandsPool setAndExecuteCommand:command];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return NSLocalizedString(@"Данные для входа", nil);
        case 1:
            return NSLocalizedString(@"Личные данные", nil);
        case 2:
            return NSLocalizedString(@"Контактная информация", nil);
        case 3:
            return NSLocalizedString(@"Наблюдаемые объединения", nil);
        default:
            return nil;
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    switch (section) {
        case 3:
            return NSLocalizedString(@"Наблюдаемые объединения - это объединения (кафедра, группа, курс, университет),"
                    " об изменении в расписании занятий которых Вы хотели бы быть уведомлены.", nil);
        default:
            return nil;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 3;
        case 1:
            return 4;
        case 2:
            return 2;
        case 3:
            return _me.readUnions.count + 1;
        default:
            return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *stringInputIdentifier = @"stringInputIdentifier";
    static NSString *unionIdentifier = @"unionIdentifier";
    static NSString *addUnionCellIdentifier = @"addUnionCellIdentifier";
    switch (indexPath.section) {
        case 0: {
            KSStringInputCell *cell = [tableView dequeueReusableCellWithIdentifier:stringInputIdentifier];
            if (!cell) {
                cell = [[KSStringInputCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                reuseIdentifier:stringInputIdentifier];
            }
            cell.object = _me;
            cell.rawTextField.secureTextEntry = NO;
            cell.rawTextField.keyboardType = UIKeyboardTypeAlphabet;
            cell.rawTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            switch (indexPath.row) {
                case 0: {
                    cell.key = @"username";
                    cell.rawTextField.placeholder = NSLocalizedString(@"Имя пользователя", nil);
                }
                break;
                case 1: {
                    cell.key = @"password";
                    cell.rawTextField.secureTextEntry = YES;
                    cell.rawTextField.placeholder = NSLocalizedString(@"Пароль", nil);
                }
                break;
                case 2: {
                    cell.key = @"passwordConfirmation";
                    cell.rawTextField.secureTextEntry = YES;
                    cell.rawTextField.placeholder = NSLocalizedString(@"Повторите пароль", nil);
                }
                break;
                default:
                    break;
            }
            return cell;
        }
        case 1: {
            KSStringInputCell *cell = [tableView dequeueReusableCellWithIdentifier:stringInputIdentifier];
            if (!cell) {
                cell = [[KSStringInputCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                reuseIdentifier:stringInputIdentifier];
            }
            cell.object = _me;
            cell.rawTextField.secureTextEntry = NO;
            cell.rawTextField.keyboardType = UIKeyboardTypeAlphabet;
            cell.rawTextField.autocapitalizationType = UITextAutocapitalizationTypeWords;
            switch (indexPath.row) {
                case 0: {
                    cell.key = @"lastName";
                    cell.rawTextField.placeholder = NSLocalizedString(@"Фамилия", nil);
                }
                    break;
                case 1: {
                    cell.key = @"firstName";
                    cell.rawTextField.placeholder = NSLocalizedString(@"Имя", nil);
                }
                    break;
                case 2: {
                    cell.key = @"givenName";
                    cell.rawTextField.placeholder = NSLocalizedString(@"Отчество", nil);
                }
                    break;
                case 3: {
                    cell.rawTextField.placeholder = NSLocalizedString(@"Тип пользователя", nil);
                    cell.rawTextField.inputView = _genderKeyboard;
                    _genderKeyboard.bindedField = cell.rawTextField;
                    cell.rawTextField.tintColor = [UIColor clearColor];
                    switch (_me.kind) {
                        case KSUserKindStudent:
                            cell.rawTextField.text = NSLocalizedString(@"Студент", nil);
                            break;
                        case KSUserKindTeacher:
                            cell.rawTextField.text = NSLocalizedString(@"Преподаватель", nil);
                            break;
                        default:
                            cell.rawTextField.text = nil;
                            break;
                    }
                }
                    break;
                default:
                    break;
            }
            return cell;
        }
        case 2: {
            KSStringInputCell *cell = [tableView dequeueReusableCellWithIdentifier:stringInputIdentifier];
            if (!cell) {
                cell = [[KSStringInputCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                reuseIdentifier:stringInputIdentifier];
            }
            cell.object = _me;
            cell.rawTextField.secureTextEntry = NO;
            cell.rawTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
            switch (indexPath.row) {
                case 0: {
                    cell.key = @"email";
                    cell.rawTextField.keyboardType = UIKeyboardTypeEmailAddress;
                    cell.rawTextField.placeholder = NSLocalizedString(@"Электронная почта", nil);
                }
                    break;
                case 1: {
                    cell.key = @"phone";
                    cell.rawTextField.keyboardType = UIKeyboardTypePhonePad;
                    cell.rawTextField.placeholder = NSLocalizedString(@"Номер телефона", nil);
                }
                    break;
                default:
                    break;
            }
            return cell;
        }
        case 3: {
            if (indexPath.row == _me.readUnions.count) {
                KSAddUnionCell *cell = [tableView dequeueReusableCellWithIdentifier:addUnionCellIdentifier];
                if (!cell) {
                    cell = [[KSAddUnionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:addUnionCellIdentifier];
                }
                cell.beginSelectionBlock = ^{
                    KSUnionSelectorController *selectorController = [[KSUnionSelectorController alloc] init];
                    selectorController.current = nil;
                    selectorController.availableUnions = _availableUnions;
                    selectorController.completionBlock = ^(KSUnion *selectedUnion) {
                        NSMutableArray *tempArr = [NSMutableArray arrayWithArray:_me.readUnions];
                        [tempArr addObject:selectedUnion];
                        [self.tableView beginUpdates];
                        _me.readUnions = [tempArr copy];
                        [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_me.readUnions.count - 1 inSection:3]]
                                              withRowAnimation:UITableViewRowAnimationAutomatic];
                        [self.tableView endUpdates];
                    };
                    [self presentViewController:[[KSNavigationController alloc] initWithRootViewController:selectorController]
                                       animated:YES completion:nil];
                };
                return cell;
            }
            else {
                KSUnionCell *cell = [tableView dequeueReusableCellWithIdentifier:unionIdentifier];
                if (!cell) {
                    cell = [[KSUnionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:unionIdentifier];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.myUnion = _me.readUnions[indexPath.row];
                return cell;
            }
        }
        default:
            return [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@""];
    }
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.section == 3 && indexPath.row < _me.readUnions.count;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSMutableArray *arr = [NSMutableArray arrayWithArray:_me.readUnions];
        [arr removeObjectAtIndex:indexPath.row];
        [self.tableView beginUpdates];
        _me.readUnions = [arr copy];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];

    }
}

-(UIView *)viewForElement:(NSObject *)element
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
    label.text = ((NSString *)element);
    label.font = [UIFont systemFontOfSize:18.0f];
    [label sizeToFit];
    return label;
}


-(void)keyboardDidChangedValue:(MTVariantsKeyboard *)keyboard {
    if ([keyboard.datasource[keyboard.selectedIndex] isEqualToString:NSLocalizedString(@"Студент", nil)]) {
        _me.kind = KSUserKindStudent;
    }
    else {
        _me.kind = KSUserKindTeacher;
    }
    switch (_me.kind) {
        case KSUserKindStudent:
            keyboard.bindedField.text = NSLocalizedString(@"Студент", nil);
            break;
        case KSUserKindTeacher:
            keyboard.bindedField.text = NSLocalizedString(@"Преподаватель", nil);
            break;
        default:
            keyboard.bindedField.text = nil;
            break;
    }
}

-(void)keyboardDidClosed:(MTVariantsKeyboard *)keyboard {
    [self keyboardDidChangedValue:keyboard];
    [keyboard.bindedField resignFirstResponder];
}

@end