//
// Created by Eugene Butusov on 05.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSLoginViewController.h"
#import "MBProgressHUD.h"
#import "KSProfileCommand.h"
#import "KSCommandsPool.h"

@interface KSLoginViewController () <UITextFieldDelegate>

@end


@implementation KSLoginViewController {
    IBOutlet UITextField *_loginField;
    IBOutlet UITextField *_passwordField;

    IBOutlet UIView *_inputView;
    IBOutlet UILabel *_errorLabel;
}

-(void)viewDidLoad {
    [super viewDidLoad];

    self.title = NSLocalizedString(@"Вход", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Войти", nil)
                                                                              style:UIBarButtonItemStylePlain target:self
                                                                             action:@selector(doLogin:)];
}

-(void)doLogin:(id)sender {
    [self.view endEditing:YES];
    __block MBProgressHUD *alert = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    alert.labelText = NSLocalizedString(@"Выполняется вход...", nil);
    KSProfileCommand *command = [KSProfileCommand authUserWithUsername:_loginField.text password:_passwordField.text
                                                      withSuccessBlock:^(KSCommonCommand *command) {
        [alert hide:YES];
        [self startApplication:nil];
    } errorBlock:^(NSError *error) {
        [alert hide:YES];
        [self showAlertWithError:error];
    }];
    [self.commandsPool setAndExecuteCommand:command];
}

-(void)showAlertWithError:(NSError *)error {
    CAKeyframeAnimation *shakingAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    shakingAnimation.values = @[[NSValue valueWithCATransform3D:CATransform3DMakeTranslation(-5.0f, 0.0f, 0.0f)], [NSValue valueWithCATransform3D:CATransform3DMakeTranslation(5.0f, 0.0f, 0.0f)]];
    shakingAnimation.autoreverses = YES ;
    shakingAnimation.repeatCount = 2.0f ;
    shakingAnimation.duration = 0.07f ;
    [_inputView.layer addAnimation:shakingAnimation forKey:nil];

    _errorLabel.text = error.localizedDescription;
    [UIView animateWithDuration:0.3f animations:^{
        _errorLabel.alpha = 1.0f;
    } completion:^(BOOL finished) {
        if (finished) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC),
                    dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.3f animations:^{
                    _errorLabel.alpha = 0.0f;
                }];
            });
        }
    }];
}

-(void)startApplication:(id)sender
{
    UIWindow *currentWindow = (UIWindow *)[UIApplication sharedApplication].windows[0];
    CATransition *animation = [CATransition animation];
    animation.type = kCATransitionReveal;
    animation.subtype = kCATransitionFromTop;

    [currentWindow.layer removeAllAnimations];
    [currentWindow.layer addAnimation:animation forKey:kCATransition];

    currentWindow.rootViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"rootView"];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _loginField) {
        [_passwordField becomeFirstResponder];
    }
    if (textField == _passwordField) {
        if (_passwordField.text.length && _loginField.text.length) {
            [self doLogin:nil];
        }
    }
    return YES;
}

@end