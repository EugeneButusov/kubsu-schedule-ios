//
// Created by Eugene Butusov on 19.09.14.
// Copyright (c) 2014 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KSCommandsPool;


@interface KSTableViewController : UITableViewController

@property (nonatomic, assign) BOOL keyboardAvoiding;
@property (nonatomic, strong) KSCommandsPool *commandsPool;

-(void)showAlertWithError:(NSError *)error;

@end