//
// Created by Eugene Butusov on 05.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSNewsViewController.h"
#import "KSNotificationCommand.h"
#import "KSCommandsPool.h"
#import "KSNewsTableViewCell.h"
#import "KSNewsDetailsViewController.h"


@implementation KSNewsViewController {
    NSArray *_dataSource;
}

-(void)viewDidLoad {
    [super viewDidLoad];

    self.title = NSLocalizedString(@"Новости", nil);
    KSNotificationCommand *command = [KSNotificationCommand notificationsOnPage:1 perPage:15 withSuccessBlock:^(KSCommonCommand *command) {
        _dataSource = (NSArray *)command.result;
        [self.tableView reloadData];
    } errorBlock:^(NSError *error) {
        [self showAlertWithError:error];
    }];
    self.keyboardAvoiding = NO;
    [self.commandsPool setAndExecuteCommand:command];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataSource.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"newsCellIdentifier";
    KSNewsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[KSNewsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.news = _dataSource[(NSUInteger)indexPath.row];
    return cell;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    KSNewsDetailsViewController *destController = (KSNewsDetailsViewController *)segue.destinationViewController;
    KSNewsTableViewCell *currentCell = (KSNewsTableViewCell *)[self.tableView cellForRowAtIndexPath:[self.tableView indexPathForSelectedRow]];
    destController.news = currentCell.news;
}

@end