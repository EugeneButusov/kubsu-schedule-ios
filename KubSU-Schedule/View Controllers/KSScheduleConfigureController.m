//
// Created by Eugene Butusov on 08.05.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSScheduleConfigureController.h"
#import "KSUnion.h"
#import "KSApplicationManager.h"
#import "KSUser.h"

@interface KSScheduleConfigureController () <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@end


@implementation KSScheduleConfigureController {
    IBOutlet UITextField *_fromField;
    IBOutlet UITextField *_toField;
    IBOutlet UITextField *_unionField;
}

-(void)viewDidLoad {
    [super viewDidLoad];

    self.title = NSLocalizedString(@"Параметры", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonItemStyleDone
                                                                                           target:self
                                                                                           action:@selector(doDone:)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                          target:self
                                                                                          action:@selector(doCancel:)];
    for (int i = 0; i < @[_fromField, _toField].count; i++) {
        UITextField *textField = @[_fromField, _toField][i];
        UIDatePicker *datePicker = [[UIDatePicker alloc] init];
        datePicker.datePickerMode = UIDatePickerModeDate;
        datePicker.tag = i;
        [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
        textField.inputView = datePicker;
    }
    UIPickerView *unionView = [[UIPickerView alloc] init];
    unionView.dataSource = self;
    unionView.delegate = self;
    _unionField.inputView = unionView;
}

-(void)setTo:(NSDate *)to {
    if (_to != to) {
        _to = to;
    }

    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateStyle = NSDateFormatterLongStyle;
    _toField.text = [dateFormatter stringFromDate:_to];
}

-(void)setFrom:(NSDate *)from {
    if (_from != from) {
        _from = from;
    }

    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateStyle = NSDateFormatterLongStyle;
    _fromField.text = [dateFormatter stringFromDate:_from];
}

-(void)setAnUnion:(KSUnion *)anUnion {
    if (_anUnion != anUnion) {
        _anUnion = anUnion;
    }

    _unionField.text = _anUnion.name;
}

-(void)doDone:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        if (self.completionHandler) {
            self.completionHandler();
        }
    }];
}

-(void)doCancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)datePickerValueChanged:(id)sender {
    switch (((UIView *)sender).tag) {
        case 0:
            self.from = ((UIDatePicker *)sender).date;
            break;
        case 1:
            self.to = ((UIDatePicker *)sender).date;
             break;
        default:
            break;
    }
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [KSApplicationManager manager].me.readUnions.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return ((KSUnion *)[KSApplicationManager manager].me.readUnions[row]).name;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.anUnion = ((KSUnion *)[KSApplicationManager manager].me.readUnions[row]);
}

@end