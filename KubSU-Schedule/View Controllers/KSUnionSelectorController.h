//
// Created by Eugene Butusov on 06.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KSTableViewController.h"

@class KSUnion;


@interface KSUnionSelectorController : KSTableViewController

@property (nonatomic, strong) void(^completionBlock)(KSUnion *selectedUnion);

@property (nonatomic, assign) NSArray *availableUnions;
@property (nonatomic, assign) KSUnion *current;

@end