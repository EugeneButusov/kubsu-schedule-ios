//
// Created by Eugene Butusov on 05.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSScheduleViewController.h"
#import "KSScheduleCommand.h"
#import "KSCommandsPool.h"
#import "KSScheduleConfigureController.h"
#import "KSNavigationController.h"
#import "KSSchedulePageView.h"
#import "KSSchedule.h"

@interface KSScheduleViewController () <UIScrollViewDelegate>

@property (nonatomic, strong) NSDate *to;
@property (nonatomic, strong) NSDate *from;
@property (nonatomic, strong) KSUnion *anUnion;

@end


@implementation KSScheduleViewController {
    IBOutlet UIScrollView *_containerView;
    IBOutlet UIPageControl *_pager;

    NSArray *_dataSource;
}

-(void)viewDidLoad {
    [super viewDidLoad];

    self.title = NSLocalizedString(@"Расписание", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"update"]
                                                                              style:UIBarButtonItemStylePlain target:self
                                                                             action:@selector(openSettings:)];
    UILabel *infoLabel = [[UILabel alloc] init];
    infoLabel.font = [UIFont boldSystemFontOfSize:17.0f];
    infoLabel.textAlignment = NSTextAlignmentCenter;
    infoLabel.textColor = [UIColor lightGrayColor];
    infoLabel.numberOfLines = 0;
    infoLabel.text = NSLocalizedString(@"Для просмотра расписания укажите, пожалуйста, временной промежуток и объедиение.", nil);
    CGSize labelSize = [infoLabel sizeThatFits:CGSizeMake(_containerView.frame.size.width - 40.0f * 2, CGFLOAT_MAX)];
    infoLabel.frame = CGRectMake((_containerView.frame.size.width - labelSize.width) / 2.0f, (_containerView.frame.size.height - labelSize.height) / 3.0f, labelSize.width, labelSize.height);
    _containerView.contentSize = _containerView.frame.size;
    _pager.numberOfPages = 0;
    [_containerView addSubview:infoLabel];
}

-(void)reloadSchedule {
    [_containerView.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [(UIView *) obj removeFromSuperview];
    }];
    _containerView.contentSize = _containerView.frame.size;
    _pager.numberOfPages = 0;
    UIActivityIndicatorView *indicatorView = [[UIActivityIndicatorView alloc]
            initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicatorView.frame = CGRectMake((_containerView.frame.size.width - indicatorView.frame.size.width) / 2.0f,
            (_containerView.frame.size.height - indicatorView.frame.size.height) / 2.0f, indicatorView.frame.size.width,
            indicatorView.frame.size.height);
    [_containerView addSubview:indicatorView];

    KSScheduleCommand *command = [KSScheduleCommand scheduleFromDate:self.from toDate:self.to anUnion:self.anUnion withSuccessBlock:^(KSCommonCommand *command) {
        _dataSource = (NSArray *)((KSScheduleCommand *)command).result;
        NSArray *dates = [[_dataSource valueForKeyPath:@"@distinctUnionOfObjects.at"]
                sortedArrayUsingDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"self" ascending:YES]]];
        _containerView.contentSize = CGSizeMake(_containerView.frame.size.width * dates.count, _containerView.frame.size.height);
        _pager.numberOfPages = dates.count;
        for (NSUInteger i = 0; i < dates.count; i++) {
            NSDate *date = dates[i];
            KSSchedulePageView *pageView = [[KSSchedulePageView alloc]
                    initWithFrame:CGRectMake(_containerView.frame.size.width * i, 0,
                            _containerView.frame.size.width, _containerView.frame.size.height)];
            pageView.at = date;
            pageView.schedules = [_dataSource objectsAtIndexes:[_dataSource indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                return [((KSSchedule *) obj).at compare:date] == NSOrderedSame;
            }]];
            [_containerView addSubview:pageView];
        }
    } errorBlock:^(NSError *error) {
        [self showAlertWithError:error];
    }];
    [self.commandsPool setAndExecuteCommand:command];
}

-(void)openSettings:(id)sender {
    KSScheduleConfigureController *configureController = [self.storyboard
            instantiateViewControllerWithIdentifier:@"configureView"];
    configureController.from = self.from;
    configureController.to = self.to;
    configureController.anUnion = self.anUnion;
    __weak KSScheduleConfigureController *weakConfigureController = configureController;
    configureController.completionHandler = ^{
        self.from = weakConfigureController.from;
        self.to = weakConfigureController.to;
        self.anUnion = weakConfigureController.anUnion;
        [self reloadSchedule];
    };
    KSNavigationController *navigationController = [[KSNavigationController alloc]
            initWithRootViewController:configureController];
    [self presentViewController:navigationController animated:YES completion:nil];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    _pager.currentPage = (NSUInteger)(_containerView.contentOffset.x / _containerView.frame.size.width);
}

@end