//
// Created by Eugene Butusov on 05.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KSUser;


@interface KSApplicationManager : NSObject

+(instancetype)manager;

@property (nonatomic, strong) NSString *sessionCookie;
@property (nonatomic, strong) KSUser *me;

@end