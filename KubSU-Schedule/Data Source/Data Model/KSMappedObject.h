//
// Created by Eugene Butusov on 05.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface KSMappedObject : NSObject

@property (nonatomic, strong) NSString *identifier;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)serialize;

@end