//
// Created by Eugene Butusov on 05.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSMappedObject.h"


@implementation KSMappedObject {

}

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super init]) {
        _identifier = dictionary[@"_id"];
    }
    return self;
}

-(NSDictionary *)serialize {
    return _identifier ? @{@"_id": _identifier} : @{};
}

@end