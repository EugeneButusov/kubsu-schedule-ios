//
// Created by Eugene Butusov on 07.05.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSSubject.h"


@implementation KSSubject {

}

@synthesize description = _description;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super initWithDictionary:dictionary]) {
        self.title = dictionary[@"title"];
        self.description = dictionary[@"description"];
    }
    return self;
}

@end