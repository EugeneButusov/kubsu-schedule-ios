//
// Created by Eugene Butusov on 07.05.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSSchedule.h"
#import "KSSubject.h"
#import "KSLessonType.h"
#import "KSUser.h"
#import "KSPlayground.h"
#import "KSUnion.h"
#import "KSLesson.h"


@implementation KSSchedule {

}

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super initWithDictionary:dictionary]) {
        NSDateFormatter *dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"yyyy-MM-dd'T'hh:mm:ss.SSS'Z'";
        self.at = [dateFormatter dateFromString:dictionary[@"at"]];
        self.subject = [[KSSubject alloc] initWithDictionary:dictionary[@"subject"]];
        self.type = [[KSLessonType alloc] initWithDictionary:dictionary[@"type"]];
        self.learningUnions = [KSUnion unionsFromArray:dictionary[@"learningUnions"]];
        self.lesson = [[KSLesson alloc] initWithDictionary:dictionary[@"lesson"]];
        self.teacher = [[KSUser alloc] initWithDictionary:dictionary[@"teacher"]];
        self.playground = [[KSPlayground alloc] initWithDictionary:dictionary[@"playground"]];
    }
    return self;
}

+(instancetype)scheduleWithDictionary:(NSDictionary *)dictionary {
    return [[self alloc] initWithDictionary:dictionary];
}

+(NSArray *)schedulesFromArray:(NSArray *)array {
    NSMutableArray *result = [NSMutableArray new];
    for (NSDictionary *dict in array) {
        KSSchedule *schedule = [KSSchedule scheduleWithDictionary:dict];
        if (schedule) {
            [result addObject:schedule];
        }
    }
    return [result copy];
}

@end