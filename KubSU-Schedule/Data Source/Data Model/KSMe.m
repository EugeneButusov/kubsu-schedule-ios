//
// Created by Eugene Butusov on 06.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSMe.h"


@implementation KSMe {

}

-(NSDictionary *)serialize {
    NSMutableDictionary *result = [[super serialize] mutableCopy];
    if (self.password) {
        result[@"password"] = self.password;
    }
    if (self.passwordConfirmation) {
        result[@"passwordConfirmation"] = self.passwordConfirmation;
    }
    return [result copy];
}

@end