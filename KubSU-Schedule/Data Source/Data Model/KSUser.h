//
// Created by Eugene Butusov on 05.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KSMappedObject.h"

typedef enum {
    KSUserKindUnspecified = 0,
    KSUserKindStudent,
    KSUserKindTeacher
} KSUserKind;

@interface KSUser : KSMappedObject

@property (nonatomic, strong) NSString *username;

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *givenName;

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *phone;

@property (nonatomic, strong) NSArray *readUnions;

@property (nonatomic, assign) KSUserKind kind;

+(instancetype)userWithDictionary:(NSDictionary *)dictionary;

@end