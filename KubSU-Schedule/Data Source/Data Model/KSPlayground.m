//
// Created by Eugene Butusov on 07.05.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSPlayground.h"


@implementation KSPlayground {

}

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super initWithDictionary:dictionary]) {
        self.name = dictionary[@"name"];
    }
    return self;
}

@end