//
// Created by Eugene Butusov on 05.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSUser.h"
#import "KSUnion.h"


@implementation KSUser {

}

-(instancetype)init {
    if (self = [super init]) {
        _readUnions = [NSArray new];
    }
    return self;
}

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super initWithDictionary:dictionary]) {
        self.username = dictionary[@"username"];
        self.email = dictionary[@"email"];
        self.firstName = dictionary[@"firstName"];
        self.lastName = dictionary[@"lastName"];
        self.givenName = dictionary[@"givenName"];
        self.kind = KSUserKindUnspecified;
        if ([dictionary[@"kind"] isEqualToString:@"student"]) {
            self.kind = KSUserKindStudent;
        }
        if ([dictionary[@"kind"] isEqualToString:@"teacher"]) {
            self.kind = KSUserKindTeacher;
        }
        if ([dictionary[@"readUnions"] isKindOfClass:[NSDictionary class]]) {
            self.readUnions = [KSUnion unionsFromArray:dictionary[@"readUnions"]];
        }

    }
    return self;
}

+(instancetype)userWithDictionary:(NSDictionary *)dictionary {
    return [[self alloc] initWithDictionary:dictionary];
}

-(NSDictionary *)serialize {
    NSMutableDictionary *result = [[super serialize] mutableCopy];
    NSArray *stringFields = @[@"username", @"firstName", @"lastName", @"givenName", @"email", @"phone"];
    for (NSString *key in stringFields) {
        if ([self valueForKey:key]) {
            result[key] = [self valueForKey:key];
        }
    }
    if (self.kind) {
        result[@"kind"] = self.kind == KSUserKindStudent ? @"student" : @"teacher";
    }
    NSMutableArray *unions = [NSMutableArray new];
    for (KSUnion *anUnion in self.readUnions) {
        [unions addObject:[anUnion serialize]];
    }
    result[@"readUnions"] = unions;

    return result;
}

@end