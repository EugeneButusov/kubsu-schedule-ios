//
// Created by Eugene Butusov on 06.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSUnion.h"


@implementation KSUnion {

}

@synthesize description = _description;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super initWithDictionary:dictionary]) {
        _name = dictionary[@"name"];
        _description = dictionary[@"description"];
        _parent = dictionary[@"parent"];
    }
    return self;
}

+(instancetype)unionWithDictionary:(NSDictionary *)dictionary {
    return [[self alloc] initWithDictionary:dictionary];
}

+(NSArray *)unionsFromArray:(NSArray *)array {
    NSMutableArray *result = [NSMutableArray new];
    for (NSDictionary *dict in array) {
        KSUnion *anUnion = [KSUnion unionWithDictionary:dict];
        if (anUnion)
            [result addObject:anUnion];
    }
    return [result copy];
}

@end