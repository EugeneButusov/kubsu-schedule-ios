//
// Created by Eugene Butusov on 21.05.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSNotification.h"
#import "KSUser.h"


@implementation KSNotification {

}

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super initWithDictionary:dictionary]) {
        NSDateFormatter *formatter = [NSDateFormatter new];
        formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        _at = [formatter dateFromString:dictionary[@"at"]];
        _title = dictionary[@"title"];
        _content = dictionary[@"content"];
        _author = [KSUser userWithDictionary:dictionary[@"author"]];
    }
    return self;
}

+(instancetype)notificationWithDictionary:(NSDictionary *)dictionary {
    return [[self alloc] initWithDictionary:dictionary];
}

+(NSArray *)notificationsFromArray:(NSArray *)array {
    NSMutableArray *result = [NSMutableArray new];
    for (NSDictionary *dict in array) {
        KSNotification *notification = [KSNotification notificationWithDictionary:dict];
        if (notification) {
            [result addObject:notification];
        }
    }
    return [result copy];
}

@end