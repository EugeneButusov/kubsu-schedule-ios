//
// Created by Eugene Butusov on 07.05.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KSMappedObject.h"


@interface KSSubject : KSMappedObject

@property (nonatomic, strong) NSString *title;
@property (atomic, copy) NSString *description;

@end