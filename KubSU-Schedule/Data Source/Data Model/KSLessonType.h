//
// Created by Eugene Butusov on 07.05.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KSMappedObject.h"


@interface KSLessonType : KSMappedObject

@property (nonatomic, strong) NSString *title;

@end