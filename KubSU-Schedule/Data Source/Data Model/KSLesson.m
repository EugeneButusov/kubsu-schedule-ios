//
// Created by Eugene Butusov on 07.05.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSLesson.h"


@implementation KSLesson {

}

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super initWithDictionary:dictionary]) {
        NSDateFormatter *dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"yyyy-MM-dd'T'hh:mm:ss.SSS'Z'";
        self.number = dictionary[@"number"];
        self.beginTime = [dateFormatter dateFromString:dictionary[@"beginTime"]];
        self.endTime = [dateFormatter dateFromString:dictionary[@"endTime"]];
    }
    return self;
}

@end