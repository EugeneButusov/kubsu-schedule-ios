//
// Created by Eugene Butusov on 07.05.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KSMappedObject.h"

@class KSSubject;
@class KSLessonType;
@class KSUser;
@class KSPlayground;
@class KSLesson;


@interface KSSchedule : KSMappedObject

@property (nonatomic, strong) NSDate *at;
@property (nonatomic, strong) KSSubject *subject;
@property (nonatomic, strong) KSLessonType *type;
@property (nonatomic, strong) NSArray *learningUnions;
@property (nonatomic, strong) KSLesson *lesson;
@property (nonatomic, strong) KSUser *teacher;
@property (nonatomic, strong) KSPlayground *playground;

+(instancetype)scheduleWithDictionary:(NSDictionary *)dictionary;
+(NSArray *)schedulesFromArray:(NSArray *)array;

@end