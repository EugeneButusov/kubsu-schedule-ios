//
// Created by Eugene Butusov on 06.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KSUser.h"


@interface KSMe : KSUser

@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *passwordConfirmation;

@end