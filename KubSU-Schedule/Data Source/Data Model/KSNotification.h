//
// Created by Eugene Butusov on 21.05.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KSMappedObject.h"

@class KSUser;


@interface KSNotification : KSMappedObject

@property (nonatomic, strong) NSDate *at;
@property (nonatomic, strong) KSUser *author;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *content;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;
+(instancetype)notificationWithDictionary:(NSDictionary *)dictionary;

+(NSArray *)notificationsFromArray:(NSArray *)array;

@end