//
// Created by Eugene Butusov on 06.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KSMappedObject.h"


@interface KSUnion : KSMappedObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) KSUnion *parent;

+(instancetype)unionWithDictionary:(NSDictionary *)dictionary;
+(NSArray *)unionsFromArray:(NSArray *)array;

@end