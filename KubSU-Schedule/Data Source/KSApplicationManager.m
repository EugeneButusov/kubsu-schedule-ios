//
// Created by Eugene Butusov on 05.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSApplicationManager.h"
#import "KSUser.h"


@implementation KSApplicationManager {

}

+(instancetype)manager {
    static KSApplicationManager *manager = nil;
    @synchronized(self)
    {
        if (manager == nil)
            manager = [[KSApplicationManager alloc] init];
    }
    return manager;
}

@end