//
// Created by Eugene Butusov on 06.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSMetadataCommand.h"
#import "KSUnion.h"

typedef enum {
    CommandTypeGetAllUnions = 0
} CommandType;

@implementation KSMetadataCommand {
    CommandType _commandType;
}

@synthesize result = _result;

-(instancetype)initGetAllUnionsWithSuccessBlock:(void (^)(KSCommonCommand *command))successBlock
                                     errorBlock:(void (^)(NSError *error))errorBlock {
    if (self = [super initWithSuccessBlock:successBlock errorBlock:errorBlock]) {
        _commandType = CommandTypeGetAllUnions;
    }
    return self;
}

+(instancetype)getAllUnionsWithSuccessBlock:(void (^)(KSCommonCommand *command))successBlock
                                 errorBlock:(void (^)(NSError *error))errorBlock {
    return [[self alloc] initGetAllUnionsWithSuccessBlock:successBlock errorBlock:errorBlock];
}

-(void)execute {
    [super execute];

    switch (_commandType) {
        case CommandTypeGetAllUnions: {
            [self generateRequestWithMethod:@"/unions" DefaultServer:YES
                             httpMethodType:HTTPMethodTypeGet params:nil dataType:SendDataTypeString headers:nil];
        }
        break;
    }
}

-(void)didFinishLoading:(NSObject *)jsonData {
    switch (_commandType) {
        case CommandTypeGetAllUnions: {
            _result = [KSUnion unionsFromArray:(NSArray *)jsonData];
            NSMutableDictionary *tempDict = [NSMutableDictionary new];
            [(NSArray *) _result enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                tempDict[((KSUnion *)obj).identifier] = obj;
            }];
            [(NSArray *) _result enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                ((KSUnion *)obj).parent = tempDict[(NSString *)((KSUnion *)obj).parent];
                tempDict[((KSUnion *)obj).identifier] = obj;
            }];
        }
        break;
    }
    if (_successBlock) {
        _successBlock(self);
    }
}

@end