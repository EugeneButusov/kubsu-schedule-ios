//
// Created by Eugene Butusov on 07.05.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KSCommonCommand.h"

@class KSUnion;


@interface KSScheduleCommand : KSCommonCommand

+(instancetype)scheduleFromDate:(NSDate *)from toDate:(NSDate *)to anUnion:(KSUnion *)anUnion
               withSuccessBlock:(void (^)(KSCommonCommand *command))successBlock
                     errorBlock:(void (^)(NSError *error))errorBlock;

@end