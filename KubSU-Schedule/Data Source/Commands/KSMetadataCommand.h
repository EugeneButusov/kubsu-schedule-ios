//
// Created by Eugene Butusov on 06.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KSCommonCommand.h"


@interface KSMetadataCommand : KSCommonCommand

+(instancetype)getAllUnionsWithSuccessBlock:(void (^)(KSCommonCommand *command))successBlock
                                 errorBlock:(void (^)(NSError *error))errorBlock;

@end