//
//  ErrorDescriptions.h
//  MysteryTrip
//
//  Created by Eugene Butusov on 13.02.14.
//  Copyright (c) 2014 VedideV. All rights reserved.
//

#define INVALID_GRANT           @"invalid_grant"
#define NOT_FOUND               @"Not Found"
#define WRONG_RECOVERY_TOKEN    @"Wrong token"