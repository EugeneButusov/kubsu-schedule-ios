//
// Created by Eugene Butusov on 21.05.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSNotificationCommand.h"
#import "KSNotification.h"

typedef enum {
    CommandTypeGetNotifications = 0
} CommandType;

@implementation KSNotificationCommand {
    NSUInteger _page, _perPage;
    CommandType _command;
}

@synthesize result = _result;

-(instancetype)initNotificationsOnPage:(NSUInteger)page perPage:(NSUInteger)perPage
                      withSuccessBlock:(void (^)(KSCommonCommand *command))successBlock
                            errorBlock:(void (^)(NSError *error))errorBlock {
    if (self = [super initWithSuccessBlock:successBlock errorBlock:errorBlock]) {
        _page = page;
        _perPage = perPage;
        _command = CommandTypeGetNotifications;
    }
    return self;
}

+(instancetype)notificationsOnPage:(NSUInteger)page perPage:(NSUInteger)perPage
                  withSuccessBlock:(void (^)(KSCommonCommand *command))successBlock
                        errorBlock:(void (^)(NSError *error))errorBlock {
    return [[self alloc] initNotificationsOnPage:page perPage:perPage withSuccessBlock:successBlock errorBlock:errorBlock];
}

-(void)execute {
    [super execute];
    switch (_command) {
        case CommandTypeGetNotifications: {
            [self generateRequestWithMethod:@"/notifications" DefaultServer:YES httpMethodType:HTTPMethodTypeGet
                                     params:nil dataType:SendDataTypeQuery headers:nil];
        }
        break;
        default:
            break;
    }
}

-(void)didFinishLoading:(NSObject *)jsonData {
    switch (_command) {
        case CommandTypeGetNotifications: {
            _result = [KSNotification notificationsFromArray:(NSArray *)jsonData];
        }
        break;
    }
    if (_successBlock) {
        _successBlock(self);
    }
}

@end