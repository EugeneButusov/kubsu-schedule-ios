//
// Created by Eugene Butusov on 07.05.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSScheduleCommand.h"
#import "KSApplicationManager.h"
#import "KSUnion.h"
#import "KSSchedule.h"

typedef enum {
    CommandTypeGetSchedule = 0
} CommandType;

@implementation KSScheduleCommand {
    CommandType _type;
    NSDictionary *_parameters;
}

@synthesize result = _result;

-(instancetype)initScheduleFromDate:(NSDate *)from toDate:(NSDate *)to anUnion:(KSUnion *)anUnion
               withSuccessBlock:(void (^)(KSCommonCommand *command))successBlock
                     errorBlock:(void (^)(NSError *error))errorBlock {
    NSParameterAssert(from && to);
    if (self = [super initWithSuccessBlock:successBlock errorBlock:errorBlock]) {
        _type = CommandTypeGetSchedule;
        _parameters = @{
                @"from": from,
                @"to": to
                //@"union": anUnion
        };
    }
    return self;
}

+(instancetype)scheduleFromDate:(NSDate *)from toDate:(NSDate *)to anUnion:(KSUnion *)anUnion
               withSuccessBlock:(void (^)(KSCommonCommand *command))successBlock
                     errorBlock:(void (^)(NSError *error))errorBlock {
    return [[self alloc] initScheduleFromDate:from toDate:to anUnion:anUnion withSuccessBlock:successBlock
                                   errorBlock:errorBlock];
}

-(void)execute {
    [super execute];
    switch (_type) {
        case CommandTypeGetSchedule: {
            NSDateFormatter *formatter = [NSDateFormatter new];
            formatter.dateFormat = @"yyyy-MM-dd'T00:00:00.000'";
            NSDictionary *params = @{
                    @"startDate": [formatter stringFromDate:_parameters[@"from"]],
                    @"endDate": [formatter stringFromDate:_parameters[@"to"]]
                    //@"union": ((KSUnion *)_parameters[@"union"]).identifier
            };
            [self generateRequestWithMethod:@"/schedules" DefaultServer:YES httpMethodType:HTTPMethodTypeGet
                                     params:params dataType:SendDataTypeQuery headers:nil];
        } break;
        default: {

        } break;
    }
}

-(void)didFinishLoading:(NSObject *)jsonData {
    switch (_type) {
        case CommandTypeGetSchedule: {
            _result = [KSSchedule schedulesFromArray:(NSArray *)jsonData];
        } break;
        default: {

        } break;
    }
    if (_successBlock) {
        _successBlock(self);
    }
}

@end