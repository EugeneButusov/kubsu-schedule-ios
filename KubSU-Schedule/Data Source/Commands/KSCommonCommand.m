//
// Created by Eugene Butusov on 05.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSCommonCommand.h"
#import "ErrorCodes.h"

static NSString *const serviceAddress = @"http://localhost:3000";

@implementation KSCommonCommand
{
    NSArray *trustedHosts;
}

-(instancetype)initWithSuccessBlock:(void (^)(KSCommonCommand *command))successBlock
                      progressBlock:(void (^)(CGFloat percentCompleted))progressBlock
                         errorBlock:(void (^)(NSError *error))errorBlock
{
    self = [super init];
    if (self)
    {
        _successBlock = successBlock;
        _errorBlock = errorBlock;
        _progressBlock = progressBlock;

        _data       = NSMutableData.data;
        _request    = nil;

        _statusCode     = 0;
        _contentLength  = 0;

        _isRunning          = NO;
        _isCancelled        = NO;
        _finishedRunning    = NO;

        trustedHosts = @[[NSString stringWithString:serviceAddress]];
    }
    return self;
}

-(instancetype)initWithSuccessBlock:(void (^)(KSCommonCommand *))successBlock errorBlock:(void (^)(NSError *))errorBlock
{
    return [self initWithSuccessBlock:successBlock progressBlock:nil errorBlock:errorBlock];
}

-(void)generateRequestWithMethod:(NSString *)method DefaultServer:(BOOL)isDefaultServer httpMethodType:(HTTPMethodType)httpMethodType params:(NSDictionary *)params dataType:(SendDataType)sendDataType headers:(NSDictionary *)requestHeaders
{
    //looking for errors
    NSParameterAssert(method.length > 0);
    if (httpMethodType == HTTPMethodTypePost || httpMethodType == HTTPMethodTypePut)
        NSParameterAssert(params != nil);

    //basic request configuration
    NSString *urlString = nil;
    if (isDefaultServer)
        urlString = [NSString stringWithFormat:@"%@%@", serviceAddress, method];
    else
        urlString = method;

    NSData *sendInfo = nil;
    switch (sendDataType)
    {
        case SendDataTypeJSON:
            sendInfo = [self generateJSONStringFromDictionary:params];
            break;
        case SendDataTypeString:
        case SendDataTypeQuery:
            sendInfo = [self generateParamsStringFromDictionary:params];
            break;
        case SendDataTypeImage:
            sendInfo = [self generateMultipartDataFromImage:[params allValues][0] withName:[params allKeys][0]];
            break;
        case SendDataTypeMultipart:
            sendInfo = [self generateMultipartDataFromDictionary:params];
            break;
        default:
            NSParameterAssert(nil);
            break;
    }

    NSString *methodString = nil;

    switch (httpMethodType)
    {
        case HTTPMethodTypeGet:
            methodString = @"GET";
            if (sendInfo != nil && sendDataType == SendDataTypeQuery)
            {
                urlString = [NSString stringWithFormat:@"%@?%@", urlString,
                                                       [NSString.alloc initWithData:sendInfo encoding:NSUTF8StringEncoding]];
            }
            break;
        case HTTPMethodTypePost:
            methodString = @"POST";
            break;
        case HTTPMethodTypePut:
            methodString = @"PUT";
            break;
        case HTTPMethodTypeDelete:
            methodString = @"DELETE";
            break;
        case HTTPMethodTypePatch:
            methodString = @"PATCH";
            break;
        default:
            NSParameterAssert(nil);
            break;
    }

    NSLog(@"send request to: %@", urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    _request = [NSMutableURLRequest requestWithURL:url];

    switch (sendDataType)
    {
        case SendDataTypeJSON:
            [_request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [_request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[sendInfo length]] forHTTPHeaderField:@"Content-Length"];
            _request.HTTPBody = sendInfo;
            break;
        case SendDataTypeString:
            [_request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [_request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[sendInfo length]] forHTTPHeaderField:@"Content-Length"];
            _request.HTTPBody = sendInfo;
            break;
        case SendDataTypeImage:
            [_request setValue:@"multipart/form-data; boundary=afwe9x8fueo9xy8rwnoer98fxer98d" forHTTPHeaderField:@"Content-Type"];
            [_request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[sendInfo length]] forHTTPHeaderField:@"Content-Length"];
            _request.HTTPBody = sendInfo;
            break;
        case SendDataTypeMultipart:
            [_request setValue:@"multipart/form-data; boundary=afwe9x8fueo9xy8rwnoer98fxer98d" forHTTPHeaderField:@"Content-Type"];
            [_request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[sendInfo length]] forHTTPHeaderField:@"Content-Length"];
            _request.HTTPBody = sendInfo;
            break;
        default:
            break;
    }
    for (NSString *key in [requestHeaders allKeys])
    {
        [_request setValue:requestHeaders[key] forHTTPHeaderField:key];
    }
    [_request setValue:@"keep-alive" forHTTPHeaderField:@"Connection"];
    _request.timeoutInterval = 50000;
    _request.HTTPMethod = methodString;
    _request.cachePolicy = NSURLRequestUseProtocolCachePolicy;
    [self createConnection];
}

-(NSData *)generateMultipartDataFromDictionary:(NSDictionary *)dictionary
{
    NSString *boundary = @"afwe9x8fueo9xy8rwnoer98fxer98d";
    NSMutableData *body = [NSMutableData data];
    for (NSString *key in dictionary.allKeys)
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        if ([dictionary[key] isKindOfClass:[NSString class]])
        {
            NSString *value = dictionary[key];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[value dataUsingEncoding:NSUTF8StringEncoding]];
        }
        if ([dictionary[key] isKindOfClass:[UIImage class]])
        {
            NSData *imageData = UIImageJPEGRepresentation(dictionary[key], 1.0);
            if (imageData)
            {
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"file.jpg\"\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:imageData];
            }
        }
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    return body;
}

-(NSData *)generateMultipartDataFromImage:(UIImage *)image withName:(NSString *)name
{
    //create instances
    NSString *boundary = @"afwe9x8fueo9xy8rwnoer98fxer98d";
    NSMutableData *body = [NSMutableData data];

    //processing
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    if (imageData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"userpic_file.jpg\"\r\n", name] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }

    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    return body;
}

-(NSData *)generateJSONStringFromDictionary:(NSDictionary *)params
{
    NSParameterAssert([NSJSONSerialization isValidJSONObject:params]);
    NSData *body = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:nil];
    return body;
}

-(NSData *)generateParamsStringFromDictionary:(NSDictionary *)params
{
    NSString *resultString = @"";
    if (!params)
        return nil;
    for (NSString *key in [params allKeys])
    {
        resultString = [NSString stringWithFormat:@"%@%@=%@&",resultString, key, params[key]];
    }
    resultString = [resultString substringToIndex:resultString.length - 1];
    resultString = CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (__bridge CFStringRef)resultString, NULL, (__bridge CFStringRef)@"!*'\"();:@+$,/?%#[]% ", CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding)));
    return [NSData dataWithBytes:[resultString UTF8String] length:[resultString length]];
}



- (void)execute
{
    NSParameterAssert(!_isRunning);
    NSParameterAssert(!_isCancelled);

    _isRunning = YES;
}

- (void)cancel
{
    //NSParameterAssert(!_isCancelled);

    _isCancelled = YES;
    _isRunning = NO;

    [_connection cancel];
    _connection = nil;
    _data       = nil;

    [self finishExecution];
}

- (void)finishExecution
{
    //NSParameterAssert(_isRunning);
    //NSParameterAssert(!_finishedRunning);

    _isRunning          = NO;
    _finishedRunning    = YES;
}

- (BOOL)isRunning
{
    return _isRunning;
}

- (BOOL)isCancelled
{
    return _isCancelled;
}

- (BOOL)finishedRunning
{
    return _finishedRunning;
}

- (void)createConnection
{
    _request.networkServiceType = NSURLNetworkServiceTypeBackground;
    _connection = [NSURLConnection.alloc initWithRequest:_request delegate:self];
}

-(BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

-(void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    NSLog(@"didReceiveAuthenticationChallenge");
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

-(void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
    if ([trustedHosts containsObject:challenge.protectionSpace.host])
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];

    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

-(void)connection:(NSURLConnection *)connection didSendBodyData:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite
{
    if (_progressBlock)
        _progressBlock((CGFloat)totalBytesWritten / (CGFloat)totalBytesExpectedToWrite);
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    _contentLength  = response.expectedContentLength;
    _data.length    = 0;
    _statusCode     = [((NSHTTPURLResponse *)response) statusCode];
    _headers        = [((NSHTTPURLResponse *)response) allHeaderFields];
}

-(NSError *)processServerError
{
    return [NSError errorWithDomain:@"Server Error" code:ERROR_SERVER_ERROR
                           userInfo:@{NSLocalizedDescriptionKey:
                                   NSLocalizedString(@"Cannot complete request. Please, try again later, or contact with support.", nil)}];
    return nil;
}

-(void)finishWithServerError
{
    NSError *error = nil;
    NSDictionary *errorObject = [NSJSONSerialization JSONObjectWithData:_data
                                                                options:NSJSONReadingAllowFragments error:nil];
    error = [NSError errorWithDomain:@"Server Error Response" code:0
                            userInfo:@{NSLocalizedDescriptionKey: errorObject[@"message"]}];
    if (_errorBlock)
        _errorBlock(error);
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_data appendData:data];
    if (_progressBlock)
        _progressBlock((CGFloat)_data.length / (CGFloat)_contentLength);
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    _isRunning = NO;
    _finishedRunning = YES;

    if (!_isCancelled)
    if (_errorBlock)
        _errorBlock([NSError errorWithDomain:@"Bad Internet Connection" code:0 userInfo:@{NSLocalizedDescriptionKey:
                NSLocalizedString(@"Looks like your internet connection is not stable.\n Please, try again in few minutes.", nil)}]);
}

- (NSObject *)parseJSON
{
    if (_data.length == 0)
        return nil;
    NSError *error = nil;
    NSObject *JSON = [NSJSONSerialization JSONObjectWithData:_data options:0 error:&error];
    /*if (error)
    {
        if (_errorBlock)
            _errorBlock(error);
        return nil;
    }*/

    return JSON;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    //NSParameterAssert(!_isCancelled);

    if (_statusCode / 100 != 2)
    {
        if (!_isCancelled)
        {
            [self finishWithServerError];
        }
        [self finishExecution];
        return;
    }

    _JSON = [self parseJSON];

    if (_JSON)
    {
        //if (((NSDictionary *)_JSON)[@"error"] != nil)
        {
            //[self finishExecution];
            //return;
        }
    }

    @try
    {
        //process to subclass
        if (!_isCancelled)
            [self didFinishLoading:_JSON];
    }
    @catch (NSException *exception)
    {
        if (!_isCancelled)
        if (_errorBlock)
            _errorBlock([NSError errorWithDomain:@"Unexpected server response"
                                            code:0
                                        userInfo:@{NSLocalizedDescriptionKey : exception.reason}]);
    }
    @finally
    {
        [self finishExecution];
    }
}


-(void)didFinishLoading:(NSObject *)jsonData
{
    NSAssert(nil, @"Must be overrided in child class.");
}

@end
