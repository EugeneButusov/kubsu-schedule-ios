//
//  ErrorCodes.h
//  MysteryTrip
//
//  Created by Eugene Butusov on 13.02.14.
//  Copyright (c) 2014 VedideV. All rights reserved.
//

#define ERROR_WRONG_PARAMETERS      1
#define ERROR_INVALID_CREDENTIALS   2
#define ERROR_NOT_FOUND             3
#define ERROR_WRONG_RECOVERY_TOKEN  4
#define ERROR_SERVER_ERROR          5
#define ERROR_UNAUTHORIZED          6

#define HTTP_BAD_REQUEST            400
#define HTTP_PAYMENT_REQUIRED       402
#define HTTP_EXPECTATION_FAILED     417
#define HTTP_OK                     200
#define HTTP_CREATED                201
#define HTTP_SERVER_ERROR           500
#define HTTP_NOT_FOUND              404
#define HTTP_FORBIDDEN              403
#define HTTP_UNAUTHORIZED           401
#define HTTP_SEE_OTHERS             303
#define HTTP_PRECONDITION_FAILED    412