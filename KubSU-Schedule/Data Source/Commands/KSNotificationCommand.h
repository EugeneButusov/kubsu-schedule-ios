//
// Created by Eugene Butusov on 21.05.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KSCommonCommand.h"


@interface KSNotificationCommand : KSCommonCommand

+(instancetype)notificationsOnPage:(NSUInteger)page perPage:(NSUInteger)perPage
                  withSuccessBlock:(void (^)(KSCommonCommand *command))successBlock
                        errorBlock:(void (^)(NSError *error))errorBlock;

@end