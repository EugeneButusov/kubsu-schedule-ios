//
// Created by Eugene Butusov on 05.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KSCommonCommand;


@interface KSCommandsPool : NSObject

-(BOOL)setAndExecuteCommand:(KSCommonCommand *)command;
-(BOOL)revokeAllCommands;
-(BOOL)revokeCommand:(KSCommonCommand *)command;
-(BOOL)revokeAllCommandsWithClass:(Class)className;

-(BOOL)isCommandRunningWithClass:(Class)className;

+(instancetype)pool;

@end