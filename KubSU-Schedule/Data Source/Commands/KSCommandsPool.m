//
// Created by Eugene Butusov on 05.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSCommandsPool.h"
#import "KSCommonCommand.h"


@implementation KSCommandsPool {
    NSMutableArray *_commands;
}

-(instancetype)init {
    self = [super init];
    if (self) {
        _commands = [NSMutableArray new];
    }
    return self;
}

+(instancetype)pool {
    return [[self alloc] init];
}

-(BOOL)setAndExecuteCommand:(KSCommonCommand *)command {
    if (command) {
        [_commands addObject:command];
        [command execute];
        return YES;
    }
    else {
        return NO;
    }
}

-(BOOL)revokeAllCommands {
    [_commands enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        KSCommonCommand *command = obj;
        if (command.isRunning)
            [command cancel];
    }];
    [_commands removeAllObjects];
    return YES;
}

-(BOOL)revokeCommand:(KSCommonCommand *)command {
    if (!command)
        return NO;
    for (NSUInteger i = 0; i < _commands.count; i++)
        if (_commands[i] == command) {
            if (command.isRunning)
                [command cancel];
            [_commands removeObjectAtIndex:i];
            return YES;
        }
    return NO;
}

-(BOOL)revokeAllCommandsWithClass:(Class)className {
    for (NSUInteger i = 0; i < _commands.count; i++)
        if ([_commands[i] isKindOfClass:className]) {
            __weak KSCommonCommand *command = _commands[i];
            if (command.isRunning)
                [command cancel];
            [_commands removeObjectAtIndex:i];
            i--;
        }
    return YES;
}

-(BOOL)isCommandRunningWithClass:(Class)className {
    for (NSUInteger i = 0; i < _commands.count; i++) {
        if ([_commands[i] isKindOfClass:className])
            return ((KSCommonCommand *)_commands[i]).isRunning;
    }
    return NO;
}

@end