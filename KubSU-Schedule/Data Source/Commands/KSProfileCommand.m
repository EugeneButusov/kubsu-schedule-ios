//
// Created by Eugene Butusov on 05.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSProfileCommand.h"
#import "KSApplicationManager.h"
#import "KSUser.h"
#import "KSMe.h"

typedef enum {
    CommandTypeAuth = 0,
    CommandTypeUpdateProfile
} CommandType;

@implementation KSProfileCommand {
    CommandType _commandType;
    NSDictionary *_parameters;
}

@synthesize result = _result;

-(instancetype)initAuthUserWithUsername:(NSString *)username password:(NSString *)password
                       withSuccessBlock:(void (^)(KSCommonCommand *command))successBlock
                             errorBlock:(void (^)(NSError *error))errorBlock {
    if (!username.length) {
        if (errorBlock) {
            errorBlock([NSError errorWithDomain:@"Wrong Parameters" code:0
                                       userInfo:@{
                                               NSLocalizedDescriptionKey: NSLocalizedString(@"Имя пользователя не может быть пустым", nil)
                                       }]);
        }
        return nil;
    }
    if (!password.length) {
        if (errorBlock) {
            errorBlock([NSError errorWithDomain:@"Wrong Parameters" code:0
                                       userInfo:@{
                                               NSLocalizedDescriptionKey: NSLocalizedString(@"Пароль не может быть пустым", nil)
                                       }]);
        }
        return nil;
    }
    if (self = [super initWithSuccessBlock:successBlock errorBlock:errorBlock]) {
        _parameters = @{
                @"username": username,
                @"password": password
        };
    }
    return self;
}

-(instancetype)initUpdateProfileForMe:(KSMe *)me withSuccessBlock:(void (^)(KSCommonCommand *command))successBlock
                           errorBlock:(void (^)(NSError *error))errorBlock {
    if (![me.password isEqualToString:me.passwordConfirmation]) {
        if (errorBlock) {
            errorBlock([NSError errorWithDomain:@"Wrong parameters" code:0
                                       userInfo:@{NSLocalizedDescriptionKey: NSLocalizedString(@"Пароли не совпадают.", nil)}]);
            return nil;
        }
    }
    if (self = [super initWithSuccessBlock:successBlock errorBlock:errorBlock]) {
        _commandType = CommandTypeUpdateProfile;
        _parameters = @{@"me": me};
    }
    return self;
}

+(instancetype)updateProfileForMe:(KSMe *)me withSuccessBlock:(void (^)(KSCommonCommand *command))successBlock
                       errorBlock:(void (^)(NSError *error))errorBlock {
    return [[self alloc] initUpdateProfileForMe:me withSuccessBlock:successBlock errorBlock:errorBlock];
}

+(instancetype)authUserWithUsername:(NSString *)username password:(NSString *)password
                   withSuccessBlock:(void (^)(KSCommonCommand *command))successBlock
                         errorBlock:(void (^)(NSError *error))errorBlock {
    return [[self alloc] initAuthUserWithUsername:username password:password withSuccessBlock:successBlock
                                       errorBlock:errorBlock];
}

-(void)execute {
    [super execute];

    switch (_commandType) {
        case CommandTypeAuth: {
            [self generateRequestWithMethod:@"/auth/signin" DefaultServer:YES httpMethodType:HTTPMethodTypePost
                                     params:_parameters dataType:SendDataTypeString headers:nil];
        }
        break;
        case CommandTypeUpdateProfile: {
            KSMe *me = _parameters[@"me"];
            [self generateRequestWithMethod:(me.identifier != nil ? @"/users" : @"/auth/signup") DefaultServer:YES
                             httpMethodType:(me.identifier != nil ? HTTPMethodTypePatch : HTTPMethodTypePost)
                                     params:[me serialize] dataType:SendDataTypeJSON headers:nil];
        }
        break;
    }
}

-(void)didFinishLoading:(NSObject *)jsonData {
    switch (_commandType) {
        case CommandTypeAuth: {
            [KSApplicationManager manager].sessionCookie = [_headers[@"Set-Cookie"] componentsSeparatedByString:@";"][0];
            [KSApplicationManager manager].me = [KSUser userWithDictionary:jsonData];
        }
        break;
        case CommandTypeUpdateProfile: {
            _result = [KSUser userWithDictionary:jsonData];
        }
        break;
    }
    if (_successBlock) {
        _successBlock(self);
    }
}

@end