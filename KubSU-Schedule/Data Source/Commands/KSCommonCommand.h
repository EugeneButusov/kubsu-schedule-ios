//
// Created by Eugene Butusov on 05.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>

#define CLIENT_ID           @"default"
#define CLIENT_SECRET       @"he2rapRA"

#define PROTOCOL_VERSION    @"2"

typedef enum {
    HTTPMethodTypePost = 0,
    HTTPMethodTypeGet,
    HTTPMethodTypePut,
    HTTPMethodTypeDelete,
    HTTPMethodTypePatch
} HTTPMethodType;

typedef enum
{
    SendDataTypeQuery = 0,
    SendDataTypeString,
    SendDataTypeJSON,
    SendDataTypeImage,
    SendDataTypeMultipart
}
        SendDataType;

@interface KSCommonCommand : NSObject <NSURLConnectionDataDelegate>
{
    NSURLConnection     *_connection;
    NSMutableURLRequest *_request;

    NSMutableData *_data;

    long long        _contentLength;
    NSInteger        _statusCode;
    NSDictionary    *_headers;

    __block void(^_errorBlock)(NSError *error);
    __block void(^_successBlock)(KSCommonCommand *command);
    __block void(^_progressBlock)(CGFloat percentCompleted);

    BOOL _isRunning;
    BOOL _finishedRunning;
    BOOL _isCancelled;

    NSObject *_JSON;
}

@property (nonatomic, readonly) NSObject *result;
@property (nonatomic, readonly) NSDictionary *basicAuthorizationHeader;

-(instancetype)initWithSuccessBlock:(void (^)(KSCommonCommand *command))successBlock
                         errorBlock:(void (^)(NSError *error))errorBlock;
-(instancetype)initWithSuccessBlock:(void (^)(KSCommonCommand *command))successBlock
                      progressBlock:(void(^)(CGFloat percentCompleted))progressBlock
                         errorBlock:(void (^)(NSError *error))errorBlock;

//request initiator (protected)
-(void)generateRequestWithMethod:(NSString *)method DefaultServer:(BOOL)isDefaultServer httpMethodType:(HTTPMethodType)httpMethodType params:(NSDictionary *)params dataType:(SendDataType)sendDataType headers:(NSDictionary *)requestHeaders;

//command managing
- (void)execute;
- (void)cancel;

// Internal use only
- (void)finishExecution;
- (void)didFinishLoading:(NSObject *)jsonData;
- (void)finishWithServerError;

//getting command state
- (BOOL)isRunning;
- (BOOL)isCancelled;
- (BOOL)finishedRunning;

// Declared explicitly in order to be invoked in the sub-classes.
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error;
-(void)connectionDidFinishLoading:(NSURLConnection *)connection;
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data;
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response;

@end
