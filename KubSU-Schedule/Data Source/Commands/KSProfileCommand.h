//
// Created by Eugene Butusov on 05.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KSCommonCommand.h"

@class KSMe;


@interface KSProfileCommand : KSCommonCommand

+(instancetype)authUserWithUsername:(NSString *)username password:(NSString *)password
                   withSuccessBlock:(void (^)(KSCommonCommand *command))successBlock
                         errorBlock:(void (^)(NSError *error))errorBlock;
+(instancetype)updateProfileForMe:(KSMe *)me withSuccessBlock:(void (^)(KSCommonCommand *command))successBlock
                                                   errorBlock:(void (^)(NSError *error))errorBlock;

@end