//
// Created by Eugene Butusov on 29.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSKeyValueCell.h"


@implementation KSKeyValueCell {
    IBOutlet UILabel *_keyLabel;
    IBOutlet UILabel *_valueLabel;
}

-(void)awakeFromNib {
    [super awakeFromNib];

    _valueLabel.adjustsFontSizeToFitWidth = YES;
}

-(void)setKey:(NSString *)key {
    if (_key != key) {
        _key = key;
    }

    _keyLabel.text = _key;
}

-(void)setValue:(NSString *)value {
    if (_value != value) {
        _value = value;
    }

    _valueLabel.text = _value;
}

@end