//
// Created by Eugene Butusov on 29.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface KSKeyValueCell : UITableViewCell

@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) NSString *value;

@end