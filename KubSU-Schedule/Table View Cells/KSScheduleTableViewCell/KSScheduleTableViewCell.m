//
// Created by Eugene Butusov on 08.05.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSScheduleTableViewCell.h"
#import "KSSchedule.h"
#import "KSSubject.h"
#import "KSLessonType.h"
#import "KSPlayground.h"
#import "KSLesson.h"
#import "KSUser.h"


@implementation KSScheduleTableViewCell {
    IBOutlet UILabel *_subjectLabel;
    IBOutlet UILabel *_typeAndPlaygroundLabel;
    IBOutlet UILabel *_timeLabel;
    IBOutlet UILabel *_teacherLabel;
    IBOutlet UILabel *_groupsLabel;
}

-(void)awakeFromNib {
    [super awakeFromNib];

    for (UILabel *label in @[_subjectLabel, _typeAndPlaygroundLabel, _timeLabel, _teacherLabel, _groupsLabel]) {
        label.adjustsFontSizeToFitWidth = YES;
    }
}

-(void)setSchedule:(KSSchedule *)schedule {
    if (_schedule != schedule) {
        _schedule = schedule;
    }

    _subjectLabel.text = _schedule.subject.title;
    _typeAndPlaygroundLabel.text = [NSString stringWithFormat:@"%@ (%@)", _schedule.type.title, _schedule.playground.name];
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateFormat = @"hh:mm";
    _timeLabel.text = [NSString stringWithFormat:@"%@ - %@", [dateFormatter stringFromDate:_schedule.lesson.beginTime],
                    [dateFormatter stringFromDate:_schedule.lesson.endTime]];
    _teacherLabel.text = [NSString stringWithFormat:@"%@ %@. %@.", _schedule.teacher.lastName,
                    [_schedule.teacher.firstName substringToIndex:1],
                    [_schedule.teacher.givenName substringToIndex:1]];
    _groupsLabel.text = [[_schedule.learningUnions valueForKeyPath:@"name"] componentsJoinedByString:@", "];
}

+(CGFloat)height {
    return 132.0f;
}

@end