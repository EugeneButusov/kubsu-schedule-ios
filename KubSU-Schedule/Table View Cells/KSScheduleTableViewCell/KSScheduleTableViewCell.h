//
// Created by Eugene Butusov on 08.05.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KSSchedule;


@interface KSScheduleTableViewCell : UITableViewCell

@property (nonatomic, strong) KSSchedule *schedule;

+(CGFloat)height;

@end