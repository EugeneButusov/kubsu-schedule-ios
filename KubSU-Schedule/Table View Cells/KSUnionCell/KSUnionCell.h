//
// Created by Eugene Butusov on 06.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KSUnion;


@interface KSUnionCell : UITableViewCell

@property (nonatomic, assign) KSUnion *myUnion;

@end