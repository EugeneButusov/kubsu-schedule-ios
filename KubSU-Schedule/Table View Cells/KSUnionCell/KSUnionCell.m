//
// Created by Eugene Butusov on 06.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSUnionCell.h"
#import "KSUnion.h"


@implementation KSUnionCell {

}

-(void)setMyUnion:(KSUnion *)myUnion {
    if (_myUnion != myUnion) {
        _myUnion = myUnion;
    }

    self.textLabel.font = [UIFont systemFontOfSize:14.0f];
    self.textLabel.text = _myUnion.name;
    self.detailTextLabel.text = _myUnion.description;
}

@end