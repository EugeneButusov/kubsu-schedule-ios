//
// Created by Eugene Butusov on 24.05.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSNewsTableViewCell.h"
#import "KSNotification.h"
#import "KSUser.h"


@implementation KSNewsTableViewCell {
    IBOutlet UILabel *_titleLabel;
    IBOutlet UILabel *_subtitleLabel;
    IBOutlet UILabel *_contentLabel;
    NSDateFormatter *_dateFormatter;
}

-(void)awakeFromNib {
    [super awakeFromNib];

    _dateFormatter = [NSDateFormatter new];
    _dateFormatter.dateFormat = @"dd.MM.yyyy HH:mm";
}

-(void)setNews:(KSNotification *)news {
    if (_news != news) {
        _news = news;
    }

    _titleLabel.text = _news.title;
    _subtitleLabel.text = [NSString stringWithFormat:@"%@ %@.%@., %@", _news.author.lastName,
                    [_news.author.firstName substringToIndex:1], [_news.author.givenName substringToIndex:1],
                    [_dateFormatter stringFromDate:_news.at]];
    _contentLabel.text = _news.content;
}

@end