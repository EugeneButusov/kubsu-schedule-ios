//
// Created by Eugene Butusov on 24.05.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KSNotification;


@interface KSNewsTableViewCell : UITableViewCell

@property (nonatomic, strong) KSNotification *news;

@end