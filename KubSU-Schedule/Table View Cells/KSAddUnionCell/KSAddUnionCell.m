//
// Created by Eugene Butusov on 06.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSAddUnionCell.h"
#import "KSUnion.h"


@implementation KSAddUnionCell {
    IBOutlet UIButton *_button;
}

-(IBAction)buttonPressed:(id)sender {
    if (self.beginSelectionBlock) {
        self.beginSelectionBlock();
    }
}

@end