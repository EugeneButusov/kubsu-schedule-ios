//
// Created by Eugene Butusov on 06.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KSUnion;


@interface KSAddUnionCell : UITableViewCell

@property (nonatomic, strong) void(^beginSelectionBlock)();

@end