//
// Created by Eugene Butusov on 06.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface KSStringInputCell : UITableViewCell

@property (nonatomic, readonly) UITextField *rawTextField;

@property (nonatomic, strong) NSObject *object;
@property (nonatomic, strong) NSString *key;

@end