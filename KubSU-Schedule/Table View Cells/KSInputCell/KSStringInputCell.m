//
// Created by Eugene Butusov on 06.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import "KSStringInputCell.h"

@interface KSStringInputCell() <UITextFieldDelegate>

@end


@implementation KSStringInputCell {
    IBOutlet UITextField *_textField;
}

-(UITextField *)rawTextField {
    return _textField;
}

-(void)prepareForReuse {
    [super prepareForReuse];
}

-(void)setupView {
    _textField.text = [self.object valueForKey:self.key];
}

-(void)setObject:(NSObject *)object {
    if (_object != object) {
        _object = object;
    }

    if (_object && _key) {
        [self setupView];
    }
}

-(void)setKey:(NSString *)key {
    if (_key != key) {
        _key = key;
    }

    if (_object && _key) {
        [self setupView];
    }
}

-(IBAction)textFieldDidChange:(UITextField *)textField {
    if (self.object && self.key) {
        [self.object setValue:textField.text forKey:self.key];
    }
}

@end