//
//  main.m
//  KubSU-Schedule
//
//  Created by Eugene Butusov on 19.09.14.
//  Copyright (c) 2014 KubSU. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KSAppDelegate class]));
    }
}
