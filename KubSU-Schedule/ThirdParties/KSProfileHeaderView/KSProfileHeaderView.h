//
// Created by Eugene Butusov on 29.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KSUser;


@interface KSProfileHeaderView : UIView

@property (nonatomic, strong) KSUser *profile;

-(instancetype)initWithTableView:(UITableView *)tableView andProfile:(KSUser *)profile;

@end