//
// Created by Eugene Butusov on 29.04.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <objc/runtime.h>
#import "KSProfileHeaderView.h"
#import "KSUser.h"

#define kHeaderHeight 150.0f

@implementation KSProfileHeaderView {
    IBOutlet UIImageView *_photoView;
    IBOutlet UILabel *_fullNameLabel;
    IBOutlet UILabel *_kindLabel;
}

-(instancetype)initWithTableView:(UITableView *)tableView andProfile:(KSUser *)profile {
    self = [[NSBundle mainBundle] loadNibNamed:[NSString stringWithCString:class_getName([self class])
                                                                  encoding:NSUTF8StringEncoding]
                                         owner:self options:nil][0];
    if (self) {
        self.frame = CGRectMake(0, 0, tableView.frame.size.width, kHeaderHeight);
        self.profile = profile;
    }
    return self;
}

-(void)setProfile:(KSUser *)profile {
    if (_profile != profile) {
        _profile  = profile;
    }

    NSMutableArray *nameArray = [NSMutableArray new];
    if (self.profile.lastName) {
        [nameArray addObject:self.profile.lastName];
    }
    if (self.profile.firstName) {
        [nameArray addObject:self.profile.firstName];
    }
    if (self.profile.givenName) {
        [nameArray addObject:self.profile.givenName];
    }
    _fullNameLabel.text = [nameArray componentsJoinedByString:@" "];
    NSString *kindString = nil;
    switch (self.profile.kind) {
        case KSUserKindStudent:
            kindString = NSLocalizedString(@"Студент", nil);
            break;
        case KSUserKindTeacher:
            kindString = NSLocalizedString(@"Преподаватель", nil);
            break;
        default:
            break;
    }
    _kindLabel.text = kindString;
}

@end