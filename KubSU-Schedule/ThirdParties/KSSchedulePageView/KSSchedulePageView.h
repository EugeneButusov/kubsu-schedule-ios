//
// Created by Eugene Butusov on 08.05.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface KSSchedulePageView : UIView

@property (nonatomic, strong) NSDate *at;
@property (nonatomic, strong) NSArray *schedules;

@end