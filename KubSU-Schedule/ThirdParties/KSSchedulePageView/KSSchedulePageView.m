//
// Created by Eugene Butusov on 08.05.15.
// Copyright (c) 2015 KubSU. All rights reserved.
//

#import <objc/runtime.h>
#import "KSSchedulePageView.h"
#import "KSScheduleTableViewCell.h"

@interface KSSchedulePageView () <UITableViewDelegate, UITableViewDataSource>

@end


@implementation KSSchedulePageView {
    IBOutlet UILabel *_dateLabel;
    IBOutlet UITableView *_containerView;

    UILabel *_emptyLabel;
}

static NSString *cellIdentifier = @"scheduleCellIdentifier";

-(instancetype)initWithFrame:(CGRect)frame {
    self = [[NSBundle mainBundle] loadNibNamed:[NSString stringWithCString:class_getName([self class])
                                                                  encoding:NSUTF8StringEncoding]
                                         owner:self options:nil][0];
    if (self) {
        self.frame = frame;
        _emptyLabel = [[UILabel alloc] init];
        _emptyLabel.font = [UIFont boldSystemFontOfSize:17.0f];
        _emptyLabel.textColor = [UIColor lightGrayColor];
        _emptyLabel.text = NSLocalizedString(@"На этот день занятий не запланировано.", nil);
        _emptyLabel.numberOfLines = 0;
        CGSize labelSize = [_emptyLabel sizeThatFits:CGSizeMake(_containerView.frame.size.width - 40.0f * 2, CGFLOAT_MAX)];
        _emptyLabel.frame = CGRectMake((_containerView.frame.size.width - labelSize.width) / 2.0f,
                (_containerView.frame.size.height - labelSize.height) / 3.0f, labelSize.width, labelSize.width);
        [_containerView addSubview:_emptyLabel];

        [_containerView registerNib:[UINib nibWithNibName:[NSString
                stringWithCString:class_getName([KSScheduleTableViewCell class]) encoding:NSUTF8StringEncoding]
                                                   bundle:[NSBundle mainBundle]]
             forCellReuseIdentifier:cellIdentifier];
    }
    return self;
}

-(void)setSchedules:(NSArray *)schedules {
    if (_schedules != schedules) {
        _schedules = schedules;
    }

    _schedules = [_schedules sortedArrayUsingDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"lesson.number"
                                                                                       ascending:YES]]];
    if (_schedules.count) {
        _containerView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _emptyLabel.hidden = YES;
    } else {
        _containerView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _emptyLabel.hidden = NO;
    }
}

-(void)setAt:(NSDate *)at {
    if (_at != at) {
        _at = at;
    }

    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    dateFormatter.dateStyle = NSDateFormatterFullStyle;
    _dateLabel.text = [dateFormatter stringFromDate:_at];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.schedules.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    KSScheduleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[KSScheduleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.schedule = self.schedules[indexPath.row];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [KSScheduleTableViewCell height];
}

@end