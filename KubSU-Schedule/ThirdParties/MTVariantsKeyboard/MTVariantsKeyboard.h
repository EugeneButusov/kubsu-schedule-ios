//
//  MTVariantsKeyboard.h
//  MysteryTrip
//
//  Created by Eugene Butusov on 17.03.14.
//  Copyright (c) 2014 VedideV. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VariantsKeyboardDelegate;



@interface MTVariantsKeyboard : UIView<UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) UITextField *bindedField;
@property (nonatomic, strong) NSArray *datasource;
@property (nonatomic, weak) id<VariantsKeyboardDelegate> delegate;

@property (nonatomic, readonly) NSInteger selectedIndex;

@end

@protocol VariantsKeyboardDelegate <NSObject>

@required
    -(UIView *)viewForElement:(NSObject *)element;

@optional
    -(void)keyboardDidChangedValue:(MTVariantsKeyboard *)keyboard;
    -(void)keyboardDidClosed:(MTVariantsKeyboard *)keyboard;

@end