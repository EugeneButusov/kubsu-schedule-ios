//
//  MTVariantsKeyboard.m
//  MysteryTrip
//
//  Created by Eugene Butusov on 17.03.14.
//  Copyright (c) 2014 VedideV. All rights reserved.
//

#import "MTVariantsKeyboard.h"

#define kPickerHeight 216.0f
#define kToolbarHeight 44.0f

@implementation MTVariantsKeyboard
{
    UIPickerView *_picker;
    UIToolbar *_toolbar;
}

- (id)initWithFrame:(CGRect)frame
{
    frame.size.height = kPickerHeight + kToolbarHeight;
    self = [super initWithFrame:frame];
    if (self)
    {
        //configure toolbar
        _toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, kToolbarHeight)];
        _toolbar.items = @[[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Готово", nil) style:UIBarButtonItemStylePlain target:self action:@selector(doComplete:)]];
        [self addSubview:_toolbar];
        
        //configure picker view
        _picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, kToolbarHeight, frame.size.width, kPickerHeight)];
        _picker.delegate = self;
        _picker.dataSource = self;
        [self addSubview:_picker];
    }
    return self;
}

-(NSInteger)selectedIndex
{
    return [_picker selectedRowInComponent:0];
}

-(void)doComplete:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(keyboardDidClosed:)])
        [self.delegate keyboardDidClosed:self];
}

#pragma mark - Picker View DataSource
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _datasource.count;
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    return [self.delegate viewForElement:_datasource[row]];
}

-(void)setDatasource:(NSArray *)datasource
{
    if (_datasource != datasource)
        _datasource = datasource;
    
    [_picker reloadAllComponents];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if ([self.delegate respondsToSelector:@selector(keyboardDidChangedValue:)])
        [self.delegate keyboardDidChangedValue:self];
}

@end
